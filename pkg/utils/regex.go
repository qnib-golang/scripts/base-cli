package utils

import (
	"fmt"
	"regexp"
)

func ReSubMatchMap(r *regexp.Regexp, str string) (res map[string]string, err error) {
	match := r.FindStringSubmatch(str)
	subMatchMap := make(map[string]string)
	subnames := r.SubexpNames()
	if len(subnames) != len(match) {
		return res, fmt.Errorf("no match found for regexp '%s' in string '%s'", r.String(), str)
	}
	for i, name := range subnames {
		if i != 0 {
			subMatchMap[name] = match[i]
		}
	}
	return subMatchMap, nil
}
